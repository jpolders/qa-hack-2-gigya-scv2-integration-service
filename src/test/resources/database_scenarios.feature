Feature: Database Scenarios

  Scenario: Test DB connection and query
    Given db credentials and a select query
    When we connect to the db credentials and execute the query
    Then we retrieve all data from this query

    Scenario: Retrieve first name giving a UID
      Given a uid 2016092314311
      When we query the database
      Then we retrieve firstname Joris

  Scenario: Retrieve first name giving a UID
    Given a uid 2016092314311
    When we query the database
    Then we retrieve firstname Tim

