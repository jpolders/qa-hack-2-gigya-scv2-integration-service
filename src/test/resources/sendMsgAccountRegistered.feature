Feature: Send message Account registered to queue
fitne
  Scenario: Send message without param
    Given there is an account_registered message
    When the account registered message is send to the queue
    Then the message is send

  Scenario Outline: Send message with param
    Given there is an account_registered message with <firstname> and <lastname>
    When the account registered message is send to the queue
    Then the message is

    Examples:
      | firstname | lastname  |
      | dorine    | vos       |
      | jeroen    | voortmans |


  Scenario Outline: Send message with param and query the database
    Given there is an account_registered message with <firstname> and <lastname>
    When the account registered message is send to the queue
    Then the message is persisted to the oracle database

    Examples:
      | firstname | lastname  |
      | dorine    | vos       |
      | jeroen    | voortmans |