package net.persgroep.testautomation.gigyascv2integrationservice.cucumber.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.persgroep.testautomation.gigyascv2integrationservice.AbstractCucumberTest;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * Created by jpolders on 27/09/2016.
 */
public class CucumberDbTests extends AbstractCucumberTest{

    private String uid;

    private List<Map<String,Object>> resultSet;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;


    @Given("^db credentials and a select query$")
    public void dbCredentialsAndASelectQuery() throws Throwable {
        jdbcTemplate.execute("SELECT * FROM ONLINE_DPG_ACCOUNT WHERE ODPG_ID = 20403");
    }

    @When("^we connect to the db credentials and execute the query$")
    public void weConnectToTheDbCredentialsAndExecuteTheQuery() throws Throwable {
    }


    @Then("^we retrieve all data from this query$")
    public void weRetrieveAllDataFromThisQuery() throws Throwable {
    }

    @Given("^a uid (.*)$")
    public void aUid(String uid) throws Throwable {
        this.uid = uid;
    }

    @When("^we query the database$")
    public void weQueryTheDatabase() throws Throwable {
        resultSet = jdbcTemplate.queryForList("Select * From online_dpg_account Where ODPG_CD = '" + uid + "'");
    }

    @Then("^we retrieve firstname (.*)$")
    public void weRetrieveFirstname(String expectedFirstname) throws Throwable {
        if (resultSet.size() == 1) {
            Assert.assertEquals(resultSet.get(0).get("FIRST_NAME"), expectedFirstname);
        }
        else {
            Assert.assertTrue(false);
        }
    }
}
