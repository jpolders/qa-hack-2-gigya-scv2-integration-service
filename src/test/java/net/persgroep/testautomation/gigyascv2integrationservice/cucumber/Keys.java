package net.persgroep.testautomation.gigyascv2integrationservice.cucumber;


public enum Keys {
    BRAND,
    GIGYA_UID,
    EMAIL,
    PASSWORD,
    RUN_BAB_BAN_CLEANUP_PROCEDURE,
    COMPLAINT_DATE
}
