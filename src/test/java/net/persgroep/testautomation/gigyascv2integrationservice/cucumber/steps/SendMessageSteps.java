package net.persgroep.testautomation.gigyascv2integrationservice.cucumber.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.persgroep.testautomation.gigyascv2integrationservice.AbstractCucumberTest;
import net.persgroep.testautomation.gigyascv2integrationservice.domain.JmsQueueSender;
import net.persgroep.testautomation.gigyascv2integrationservice.domain.gigyamessages.AccountRegistered;
import org.apache.commons.net.ntp.TimeStamp;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.jms.ObjectMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;


public class SendMessageSteps extends AbstractCucumberTest {

    String msg;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    AccountRegistered accRegMessage;

    @Autowired
    JmsQueueSender messageSender;

    private List<Map<String,Object>> resultSet;

    @Given("^there is an account_registered message$")
    public void thereIsAnAccount_registeredMessage() throws Throwable {
        accRegMessage.setFirstName("Jelle");
        accRegMessage.setLastName("Polders");
        accRegMessage.setLockedOut(false);
        accRegMessage.addPhone("+31204961668");
        accRegMessage.addPhone("+31203310839");
        msg = accRegMessage.createJson();
    }

    @When("^the account registered message is send to the queue$")
    public void theAccountRegisteredMessageIsSendToTheQueue() throws Throwable {
        messageSender.sendMessage(msg);
        try {
            String filename = "account_registered_" + System.currentTimeMillis() + ".json";
            File accountRegisteredMsgFile = new File(System.getProperty("user.dir") + "/input/" + filename);
            System.out.println(accountRegisteredMsgFile);
            FileWriter fw = new FileWriter(accountRegisteredMsgFile);
            fw.write(msg);
            fw.close();
        } catch (IOException iox) {
            iox.printStackTrace();
        }
    }

    @Then("^the message is send$")
    public void theMessageIsSend() throws Throwable {
        System.out.println("VALIDATIE VAN JELLE EN TIM");
    }

    @Given("^there is an account_registered message with (.*) and (.*)$")
    public void thereIsAnAccount_registeredMessageWithFirstnameAndLastname(String firstname, String lastname) throws Throwable {
        accRegMessage.generateUID();
        accRegMessage.setFirstName(firstname);
        accRegMessage.setLastName(lastname);
        accRegMessage.setLockedOut(false);
        accRegMessage.addPhone("+31204961668");
        accRegMessage.addPhone("+31203310839");
        msg = accRegMessage.createJson();
    }

    @Then("^the message is$")
    public void theMessageIs() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the message is persisted to the oracle database$")
    public void theMessageIsPersistedToTheOracleDatabase() throws Throwable {
        Thread.sleep(2000);
        resultSet = jdbcTemplate.queryForList("Select FIRST_NAME, LAST_NAME From online_dpg_account Where ODPG_CD = '" + accRegMessage.getUID() + "'");

        Assert.assertEquals(resultSet.size(), 1);

        Assert.assertEquals(resultSet.get(0).get("FIRST_NAME"), accRegMessage.getFirstName());
        Assert.assertEquals(resultSet.get(0).get("LAST_NAME"), accRegMessage.getLastName());
    }
}
