package net.persgroep.testautomation.gigyascv2integrationservice;

import net.persgroep.testautomation.gigyascv2integrationservice.config.ActiveMQConfig;
import net.persgroep.testautomation.gigyascv2integrationservice.config.CucumberTestConfig;
import net.persgroep.testautomation.gigyascv2integrationservice.config.JdbcTemplateConfiguration;
import net.persgroep.testautomation.gigyascv2integrationservice.config.OracleConfiguration;
import net.persgroep.testautomation.gigyascv2integrationservice.cucumber.CucumberContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = {CucumberTestConfig.class, OracleConfiguration.class, JdbcTemplateConfiguration.class, ActiveMQConfig.class},
        loader = SpringApplicationContextLoader.class
)
public abstract class AbstractCucumberTest {

    @Autowired
    protected CucumberContext cucumbercontext;
}
