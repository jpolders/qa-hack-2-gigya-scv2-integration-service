package net.persgroep.testautomation.gigyascv2integrationservice.domain;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.JmsTemplate;


public class JmsQueueSender {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMessage(final String msg) {

        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMessage objectMessage = session.createObjectMessage(msg);
                return objectMessage;
            }
        });
    }


}
