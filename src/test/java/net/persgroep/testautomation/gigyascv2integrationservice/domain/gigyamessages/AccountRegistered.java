package net.persgroep.testautomation.gigyascv2integrationservice.domain.gigyamessages;

import net.persgroep.testautomation.gigyascv2integrationservice.domain.GigyaMessage;

import java.util.ArrayList;
import java.util.List;


public class AccountRegistered implements GigyaMessage {

    private boolean isLockedOut;
    private boolean isVerified;
    private long createdTimestamp;
    private String firstName;
    private String lastName;

    private int birthDay;
    private int birthMonth;
    private String gender;
    private int birthYear;
    private String nickname;
    private ArrayList<String> phones = new ArrayList<String>();
    private String email;
    private boolean isActive;
    private String UID;
    private boolean isRegistered;
    private List<String> loginIdsEmails;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void generateUID() {
        UID =  "" + System.currentTimeMillis();
    }

    public String getUID() {
        return UID;
    }

    public void addPhone(String phone){
        phones.add(phone);

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public boolean isLockedOut() {
        return isLockedOut;
    }
    public void setLockedOut(boolean lockedOut) {
        isLockedOut = lockedOut;
    }

    public String getPhoneList(){
        String listOfPhones = "";

        if(phones.size()!= 0){
            for(int i=0; i<phones.size() -1; i++){
                listOfPhones += "{\"number\": \"" + phones.get(i) + "\"},\n";
            }
            listOfPhones += "{\"number\": \"" + phones.get(phones.size()-1) + "\"}\n";
        }
        return listOfPhones;
    }

    public String createJson() {
        String jsonMsg = "{\n" +
                "  \"accountInfo\": {\n" +
                "    \"isLockedOut\":" + isLockedOut +",\n" +
                "    \"lastLogin\": \"Thu Jan 01 01:00:01 CET 1990\",\n" +
                "    \"lastUpdatedTimestamp\": 1386323200,\n" +
                "    \"data\": {\"key-43\": \"value-44\"},\n" +
                "    \"isVerified\": true,\n" +
                "    \"created\": \"Thu Jan 05 01:00:01 CET 1990\",\n" +
                "    \"createdTimestamp\": 1286323200000,\n" +
                "    \"profile\": {\n" +
                "      \"firstName\": \"" + firstName + "\",\n" +
                "      \"lastName\": \"" + lastName + "\",\n" +
                "      \"birthDay\": 8,\n" +
                "      \"birthMonth\": 2,\n" +
                "      \"gender\": \"m\",\n" +
                "      \"birthYear\": 1985,\n" +
                "      \"nickname\": \"DORINE\",\n" +
                "      \"phones\": [\n" + getPhoneList() +
                "      ],\n" +
                "      \"email\": \"f2hjamantje@hetnet.nl\"\n" +
                "    },\n" +
                "    \"isActive\": false,\n" +
                "    \"UID\": \"" + UID + "\",\n" +
                "    \"password\": {\n" +
                "      \"hashSettings\": {\n" +
                "        \"salt\": \"salt-10\",\n" +
                "        \"rounds\": 9,\n" +
                "        \"algorithm\": \"algorithm-8\"\n" +
                "      },\n" +
                "      \"hash\": \"hash-7\"\n" +
                "    },\n" +
                "    \"identities\": [\n" +
                "      {\n" +
                "        \"firstName\": \"first_name-21\",\n" +
                "        \"lastName\": \"last_name-22\",\n" +
                "        \"profilePicture\": \"profile_picture-24\",\n" +
                "        \"provider\": \"TWITTER\",\n" +
                "        \"allowsLogin\": true,\n" +
                "        \"nickname\": \"nickname-23\",\n" +
                "        \"email\": \"email-20\",\n" +
                "        \"providerUID\": \"providerUID-19\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"firstName\": null,\n" +
                "        \"lastName\": null,\n" +
                "        \"profilePicture\": null,\n" +
                "        \"provider\": \"FACEBOOK\",\n" +
                "        \"allowsLogin\": false,\n" +
                "        \"nickname\": null,\n" +
                "        \"email\": null,\n" +
                "        \"providerUID\": \"DATALOAD56\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"isRegistered\": false,\n" +
                "    \"loginIDs\": {\"emails\": [\n" +
                "      \"janneliesd@hotmail.com\",\n" +
                "      \"f2hjamantje@hetnet.nl\",\n" +
                "      \"ruth.spaargaren@planet.nl\"\n" +
                "    ]}\n" +
                "  },\n" +
                "  \"uid\": \"201609231431\",\n" +
                "  \"id\": \"04be19bd-25fe-415c-201602111420\",\n" +
                "  \"type\": \"accountRegistered\",\n" +
                "  \"timestamp\": 1386323200\n" +
                "}";
        return jsonMsg;

    }

}
