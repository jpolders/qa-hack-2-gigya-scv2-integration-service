package net.persgroep.testautomation.gigyascv2integrationservice.config;

import net.persgroep.testautomation.gigyascv2integrationservice.cucumber.CucumberContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import net.persgroep.testautomation.gigyascv2integrationservice.domain.JmsQueueSender;
import net.persgroep.testautomation.gigyascv2integrationservice.domain.gigyamessages.AccountRegistered;

@SpringBootApplication
public class CucumberTestConfig {

    @Bean
    public CucumberContext cucumberContext() {
        return new CucumberContext();
    }

    @Bean
    public JmsQueueSender jmsQueueSender(){return new JmsQueueSender();}

    @Bean
    public AccountRegistered accountRegistered() {return new AccountRegistered();}

}
